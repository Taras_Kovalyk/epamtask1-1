﻿using System;
using System.Text;

namespace MathEntities
{
    public class Matrix
    {
        private int _rows;
        private int _columns;
        private double[,] _matrix;

        public double this[int rowIndex, int colIndex]
        {
            get { return _matrix[rowIndex, colIndex]; }
            set { _matrix[rowIndex, colIndex] = value; }
        }
        public int Rows
        {
            get { return _rows; }
        }
        public int Columns
        {
            get { return _columns; }
        }
        /// <summary>
        /// Creates instance of matrix by following size.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        public Matrix(int rows, int columns)
        {
            if (rows <= 0 || columns <= 0)
            {
                throw new ArgumentException("Rows and columns count must be biger then null");
            }
            _rows = rows;
            _columns = columns;
            _matrix = new double[rows, columns];
        }
        /// <summary>
        /// Creates instance of Matrix from two-demensional array.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="matrix"></param>
        public Matrix(double[,] matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException("Matrix cannot be null");
            }
            _rows = matrix.GetLength(0);
            _columns = matrix.GetLength(1);
            _matrix = matrix;
        }
        private Matrix DoArithmetic(Matrix matrix1, Matrix matrix2, bool isAdd)
        {
            if (matrix1 == null || matrix2 == null)
            {
                throw new ArgumentNullException("Arguments cannot be null");
            }
            if (matrix1.Rows != matrix2.Rows || matrix1.Columns != matrix2.Columns)
            {
                throw new ArgumentException("Matrices should have the same size");
            }
            double[,] sum = matrix1._matrix;
            if (isAdd)
            {
                for (int i = 0; i < sum.GetLength(0); i++)
                {
                    for (int j = 0; j < sum.GetLength(1); j++)
                    {
                        sum[i, j] += matrix2[i,j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < sum.GetLength(0); i++)
                {
                    for (int j = 0; j < sum.GetLength(1); j++)
                    {
                        sum[i, j] -= matrix2[i, j];
                    }
                }
            }
            return new Matrix(sum);
        }
        /// <summary>
        /// Adds two matrices.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns></returns>
        public Matrix Add(Matrix matrix1, Matrix matrix2)
        {
            return this.DoArithmetic(matrix1, matrix2, true);
        }
        /// <summary>
        /// Substracts two matrices.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns></returns>
        public Matrix Sustract(Matrix matrix1, Matrix matrix2)
        {
            return this.DoArithmetic(matrix1, matrix2, false);
        }
        /// <summary>
        /// Multipies two matrices.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <returns></returns>
        public Matrix Multiply(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null || matrix2 == null)
            {
                throw new ArgumentNullException("Arguments cannot be null");
            }
            if (matrix1.Rows != matrix2.Columns || matrix1.Columns != matrix2.Rows)
            {
                throw new ArgumentException("Matrix1 rows count must be equals to matrix2 column count");
            }
            
            double[,] result = new double[matrix1.Rows, matrix2.Columns];

            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 0; j < result.GetLength(1); j++)
                {
                    for (int k = 0; k < matrix1._matrix.GetLength(1); k++)
                    {
                        result[i, j] += matrix1[i, k] * matrix2[k, j];
                    }
                }
            }
            return new Matrix(result);
        }
        /// <summary>
        /// Gets determinant.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        public double GetDeterminant()
        {
            return this.GetDeterminant(this._matrix);
        }

        private double GetDeterminant(double[,] matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException("Matrix cannot be null");
            }
            if (matrix.GetLength(0) != matrix.GetLength(1))
            {
                throw new ArgumentException("Determinant can be calculated for square matrices only");
            }

            int length = matrix.GetLength(0);
            double determinant;
            double[,] subMatrix;

            if(length == 1)
            {
                determinant = matrix[0, 0];
            }
            else if(length == 2)
            {
                determinant = matrix[0, 0] * matrix[1, 1]
                    - matrix[1, 0] * matrix[0, 1];
            }
            else
            {
                determinant = 0;
                for (int minorColIndex = 0; minorColIndex < length; minorColIndex++)
                {
                    subMatrix = new double[length - 1, length - 1];

                    for (int rowIndex = 1; rowIndex < length; rowIndex++)
                    {
                        int subColIndex = 0;
                        for (int colIndex = 0; colIndex < length; colIndex++)
                        {
                            if (colIndex != minorColIndex)
                            {
                                subMatrix[rowIndex - 1, subColIndex] = matrix[rowIndex, colIndex];
                                subColIndex++;
                            }
                        }
                    }
                    determinant += Math.Pow(-1, minorColIndex + 2) * matrix[0, minorColIndex]
                        * GetDeterminant(subMatrix);
                }
            }
            return determinant;
        }
        /// <summary>
        /// Calculates order minor.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <param name="order">Order</param>
        /// <returns></returns>
        public double GetOrderMinor(int order)
        {
            if (order < 0 || order > this._columns || order > this._rows)
            {
                throw new ArgumentException("Order cannot be nagetive or biger then matrix size.");
            }

            double[,] subMatrix = new double[order, order];
            for (int i = 0; i < order; i++)
            {
                for (int j = 0; j < order; j++)
                {
                    subMatrix[i, j] = this[i, j];
                }
            }
            return this.GetDeterminant(subMatrix);
        }
        /// <summary>
        /// Gets additional minor for order minor of following order.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <param name="order">Order</param>
        /// <returns></returns>
        public double GetAdditionalMinor(int order)
        {
            if (this._rows != this._columns)
            {
                throw new ArgumentException("This matrix should be square.");
            }
            if (order < 0 || order > this._columns || order > this._rows)
            {
                throw new ArgumentException("Order cannot be nagetive or biger then matrix size.");
            }
            int subLength = this._rows - order + 1;
            double[,] subMatrix = new double[subLength, subLength];
            for (int i = order - 1; i < this._rows; i++)
            {
                for (int j = order - 1; j < this._rows; j++)
                {
                    subMatrix[i - order + 1, j - order + 1] = this[i, j];
                }
            }
            return this.GetDeterminant(subMatrix);
        }
        /// <summary>
        /// Gets element minor.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <param name="rowIndex">Row index of element</param>
        /// <param name="colIndex">Column index of element</param>
        /// <returns></returns>
        public double GetElementMinor(int rowIndex, int colIndex)
        {
            if (this._rows != this._columns)
            {
                throw new ArgumentException("This matrix should be square.");
            }
            if (rowIndex < 0 || colIndex < 0 || rowIndex >= this._rows || colIndex >= this._columns)
            {
                throw new ArgumentException("Arguments cannot be biger then matrix size");
            }

            double[,] subMatrix = new double[this._rows-1, this._rows-1];
            for (int i = 0; i < subMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < subMatrix.GetLength(1); j++)
                {
                    if (i != rowIndex || j != colIndex)
                    {
                        subMatrix[i, j] = this[i, j];
                    }
                }
            }
            return GetDeterminant(subMatrix);
        }
        public override string ToString()
        {
            StringBuilder result = new StringBuilder(_matrix.Length * 3);
            result.Append("\n");
            for (int i = 0; i < _matrix.GetLength(0); i++)
            {
                for (int j = 0; j < _matrix.GetLength(1); j++)
                {
                    result.Append($"{_matrix[i,j]} ");
                }
                result.Append("\n");
            }
            return result.ToString();
        }
    }
}
