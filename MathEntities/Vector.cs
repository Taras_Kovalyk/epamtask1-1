﻿using System;
using System.Text;

namespace MathEntities
{
    public class Vector: IComparable<Vector>
    {
        private int[] _vec;
        private int _startIndex;

        public int this[int index]
        {
            get
            {
                if (index < _startIndex)
                {
                    throw new IndexOutOfRangeException("Index are lower then start index.");
                }
                return _vec[index - _startIndex];
            }
            set
            {
                if (index < _startIndex)
                {
                    throw new IndexOutOfRangeException("Index are lower then start index.");
                }
                _vec[index - _startIndex] = value;
            }
        }
        public int Count
        {
            get { return _vec.Length; }
        }
        public int LastIndex
        {
            get { return _startIndex + _vec.Length - 1; }
        }

        public int StartIndex
        {
            get { return _startIndex; }
        }
        /// <summary>
        /// Creates instance of Vector and assigns values from array.
        /// </summary>
        /// <param name="startIndex">Start index</param>
        /// <param name="array">Values to assing</param>
        public Vector(int startIndex, int[] array)
        {
            _startIndex = startIndex;
            _vec = array;
        }
        /// <summary>
        /// Creates instance of Vector with interval from startIndex to endIndex.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        public Vector(int startIndex, int endIndex)
        {
            if (endIndex <= startIndex)
            {
                throw new ArgumentException("endIndex mast be biger then start index");
            }
            _startIndex = startIndex;
            _vec = new int[endIndex - startIndex + 1];
        }
        /// <summary>
        /// Returns Vector - result of adding following vectors.
        /// Vectors should have the same intervals length.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public Vector Add(Vector vec1, Vector vec2)
        {
            if (vec1.Count != vec2.Count)
            {
                throw new ArgumentException("The arguments have diferent interval lengths");
            }

            Vector result = new Vector(vec1.StartIndex, vec1.LastIndex);
            int j = vec2.StartIndex;
            for (int i = vec1.StartIndex; i <= vec1.LastIndex; i++)
            {
                result[i] = vec1[i] + vec2[j];
                j++;
            }
            return result;
        }
        /// <summary>
        /// Returns Vector - result of substracting following vectors.
        /// Vectors should have the same intervals length.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public Vector Substract(Vector vec1, Vector vec2)
        {
            if (vec1.Count != vec2.Count)
            {
                throw new ArgumentException("The arguments have diferent interval lengths");
            }

            Vector result = new Vector(vec1.StartIndex, vec1.LastIndex);
            int j = vec2.StartIndex;
            for (int i = vec1.StartIndex; i <= vec1.LastIndex; i++)
            {
                result[i] = vec1[i] - vec2[j];
                j++;
            }
            return result;
        }
        /// <summary>
        /// Multiplies each value of this Vector on the multiplier.
        /// </summary>
        /// <param name="multiplier"></param>
        /// <returns>Returns result Vector</returns>
        public Vector Multiply(int multiplier)
        {
            Vector result = new Vector(this.StartIndex, this.LastIndex);
            for (int i = this.StartIndex; i <= this.LastIndex; i++)
            {
                result[i] = this[i] * multiplier;
            }

            return result;
        }
        /// <summary>
        /// Compares two Vectors by Count property.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Vector other)
        {
            if (other == null)
            {
                return 1;
            }
            return this.Count.CompareTo(other.Count);

        }
        public override string ToString()
        {
            StringBuilder result = new StringBuilder(this.Count * 3);
            for (int i = 0; i < _vec.Length; i++)
            {
                result.Append($" {_vec[i]},");
            }
            result.Remove(result.Length - 1, 1);
            return result.ToString();
        }
    }
}
