﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MathEntities
{
    public class Rectangle
    {
        private int _width;
        private int _height;

        private Point[] _points;

        private bool IsRectangleValid(Point[] points)
        {
            bool result = points[0].X == points[1].X 
                && points[2].X == points[3].X
                && points[0].X < points[3].X
                && points[0].Y == points[3].Y
                && points[1].Y == points[2].Y
                && points[0].Y < points[1].Y;
            return result;
        }
        private void CalculateSize()
        {
            _width  = Math.Abs(_points[3].X - _points[0].X);
            _height = Math.Abs(_points[1].Y - _points[0].Y);
        }

        /// <summary>
        /// Creates an instance of Rectangle from coordinates arrays.
        /// The count begins from bottom left angle. 
        /// </summary>
        /// <exception cref="ArgumentException"/>
        /// <param name="xCoordinates">X axis coordinates array. Requirment array length = 4</param>
        /// <param name="yCoordinates">Y axis coordinates array. Requirment array length = 4</param>
        public Rectangle(int[] xCoordinates, int[] yCoordinates)
        {
            Point[] tempPoints = new Point[4];
            if (xCoordinates.Length == 4 && yCoordinates.Length == 4)
            {
                for (int i = 0; i < _points.Length; i++)
                {
                    tempPoints[i] = new Point(xCoordinates[i], yCoordinates[i]);
                }
                if (IsRectangleValid(tempPoints))
                {
                    _points = tempPoints;
                    CalculateSize();
                }
                else
                {
                    throw new ArgumentException("Rectangle cannot be created with passed arguments");
                }
            }
            else
            {
                throw new ArgumentException("The length of xCoordinates and yCoordinates must be equals 4");
            }
        }

        /// <summary>
        /// Creates an instance of Rectangle from the following coordinates pairs.
        /// The count begins from bottom left angle.
        /// </summary>
        /// <exception cref="ArgumentException"/>
        /// <param name="leftXCoord">Left X coordinate</param>
        /// <param name="rightXCoord">Right X coordinate</param>
        /// <param name="bottomYCoord">Bottom Y coordinate</param>
        /// <param name="topYCoord">Top Y coordinate</param>
        public Rectangle(int leftXCoord, int rightXCoord, int bottomYCoord, int topYCoord)
        {
            Point[] tempPoints = new Point[4];
            tempPoints[0] = new Point(leftXCoord, bottomYCoord);
            tempPoints[1] = new Point(leftXCoord, topYCoord);
            tempPoints[2] = new Point(rightXCoord, topYCoord);
            tempPoints[3] = new Point(rightXCoord, bottomYCoord);
            if (IsRectangleValid(tempPoints))
            {
                _points = tempPoints;
                CalculateSize();
            }
            else
            {
                throw new ArgumentException("Rectangle cannot be created with passed arguments");
            }
        }

        /// <summary>
        /// Creates an instance of Rectangle from the following IEnumerable<Point>.
        /// The count begins from bottom left angle.
        /// </summary>
        /// <exception cref="ArgumentException"/>
        /// <param name="points">IEnumerable<Point>. Requirment length = 4</param>
        public Rectangle(IEnumerable<Point> points)
        {
            if (points.Count() == 4)
            {
                Point[] tempPoints = points.ToArray();
                if (IsRectangleValid(tempPoints))
                {
                    _points = tempPoints;
                    CalculateSize();
                }
                else
                {
                    throw new ArgumentException("Rectangle cannot be created with passed arguments");
                }
            }
            else
            {
                throw new ArgumentException("The length of IEnumerable<Point> must be equals 4");
            }
        }

        public Point[] Points
        {
            get { return _points; }
        }
        public int Width
        {
            get
            {
               return _width;
            }
        }
        public int Height
        {
            get
            {
                return _height;
            }
        }
        /// <summary>
        /// Increases current coordinates on following values.
        /// </summary>
        /// <param name="xStep">Increasing step of X coordinate.</param>
        /// <param name="yStep">Increasing step of Y coordinate.</param>
        public void Move(int xStep, int yStep)
        {
            for (int i = 0; i < _points.Length; i++)
            {
                _points[i].X += xStep;
                _points[i].Y += yStep;
            }
        }
        /// <summary>
        /// Changes current rectangle width.
        /// </summary>
        /// <param name="changeStep">Step to change Width.</param>
        /// <param name="fromLeftSide">Defines from which side Width will change.</param>
        /// <returns>Returns true if width was changed.</returns>
        public bool ChangeWidth(int changeStep, bool fromLeftSide = true)
        {
            if (_width + changeStep <= 0)
            {
                return false;
            }
            if (fromLeftSide)
            {
                _points[0].X += changeStep;
                _points[1].X += changeStep;
                _width += changeStep;
                return true;
            }
            else
            {
                _points[2].X += changeStep;
                _points[3].X += changeStep;
                _width += changeStep;
                return true;
            }
        }
        /// <summary>
        /// Changes current rectangle height.
        /// </summary>
        /// <param name="changeStep">Step to change Height</param>
        /// <param name="fromBottomSide">Defines from which side Height will change.</param>
        /// <returns>Returns true if Height was changed.</returns>
        public bool ChangeHeight(int changeStep, bool fromBottomSide = true)
        {
            if (_height + changeStep <= 0)
            {
                return false;
            }
            if (fromBottomSide)
            {
                _points[0].Y += changeStep;
                _points[3].Y += changeStep;
                _height += changeStep;
                return true;
            }
            else
            {
                _points[1].Y += changeStep;
                _points[2].Y += changeStep;
                _height += changeStep;
                return true;
            }
        }
        /// <summary>
        /// Builds the smallest rectangle that consists of two following rectangles.
        /// </summary>
        /// <param name="rect1">First rectangle</param>
        /// <param name="rect2">Second rectangle</param>
        /// <returns>Returns the instance of a Rectangle.</returns>
        public Rectangle BuildSmallestRectangle(Rectangle rect1, Rectangle rect2)
        {
            int leftX   = int.MaxValue;
            int rightX  = int.MinValue;
            int bottomY = int.MaxValue;
            int topY    = int.MinValue;

            Point[] points = new Point[8];

            rect1.Points.CopyTo(points, 0);
            rect2.Points.CopyTo(points, 4);

            foreach (var point in points)
            {
                leftX = leftX > point.X ? point.X : leftX;
                rightX = rightX < point.X ? point.X : rightX;
                bottomY = bottomY > point.Y ? point.Y : bottomY;
                topY = topY < point.Y ? point.Y : topY;
            }

            return new Rectangle(leftX, rightX, bottomY, topY);
        }
        private bool IsRectanglesCrossing(Rectangle rect1, Rectangle rect2)
        {
            var rectangle = BuildSmallestRectangle(rect1, rect2);
            int width = rect1.Width + rect2.Width;
            int height = rect1.Height + rect2.Height;
            return width > rectangle.Width && height > rectangle.Height;
        }
        /// <summary>
        /// Builds a rectangle - result of crossing two following rectangles.
        /// </summary>
        /// <param name="rect1">First rectangle</param>
        /// <param name="rect2">Second rectangle</param>
        /// <returns>Returns an instance of Rectangle or null if rectangles are not crossed.</returns>
        public Rectangle BuildCrossedRectangle(Rectangle rect1, Rectangle rect2)
        {
            if (IsRectanglesCrossing(rect1, rect2))
            {
                var firstPoints = rect1.Points;
                var secondPoints = rect2.Points;
                int[] xCoords = { firstPoints[0].X, firstPoints[3].X,
                                  secondPoints[0].X, secondPoints[3].X};
                int[] yCoords = { firstPoints[0].Y, firstPoints[1].Y,
                                  secondPoints[0].Y, secondPoints[1].Y};

                Array.Sort(xCoords);
                Array.Sort(yCoords);

                int leftX = xCoords[1];
                int rightX = xCoords[2];
                int bottomY = yCoords[1];
                int topY = yCoords[2];
                return new Rectangle(leftX, rightX, bottomY, topY);
            }
            else
            {
                return null;
            }
        }
    }
}
