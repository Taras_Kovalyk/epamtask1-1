﻿using MathEntities;
using System;
using static System.Console;

namespace ConsoleDemo
{
    class Program
    {

        static void RectangleDemo()
        {
            WriteLine("------Reactangle demo--------------");
            Rectangle rect = new Rectangle(-1, 4, 0, 2);
            WriteLine("Points: ");
            foreach (var item in rect.Points)
            {
                WriteLine(item.ToString());
            }
            WriteLine($"width: {rect.Width}, heigth: {rect.Height}");
            Rectangle rect1 = new Rectangle(-4, -2, -1, 3);
            var smallestRect = rect.BuildSmallestRectangle(rect, rect1);
            WriteLine("Points of smallest: ");
            foreach (var item in smallestRect.Points)
            {
                WriteLine(item.ToString());
            }
            Rectangle rect2 = new Rectangle(-3, 0, -2, 2);
            var crossedRect = rect1.BuildCrossedRectangle(rect1, rect2);
           
            if (crossedRect != null)
            {
                WriteLine("Points of crossed: ");
                foreach (var item in crossedRect.Points)
                {
                    WriteLine(item.ToString());
                }
                WriteLine($"width: {crossedRect.Width}, heigth: {crossedRect.Height}");
            }
            crossedRect.ChangeWidth(3);
            WriteLine("Points of crossed with changed width: ");
            foreach (var item in crossedRect.Points)
            {
                WriteLine(item.ToString());
            }
            WriteLine($"width: {crossedRect.Width}, heigth: {crossedRect.Height}");


        }

        static void PolynomialDemo()
        {
            WriteLine("----Polynomial demo------");
            double[] a = { 1, 4, 5 };
            double[] b = { 3, 1, 2 };
            double[] c = { 5, 3, 1, 6 };
            Polynomial polynom1 = new Polynomial(a);
            Polynomial polynom2 = new Polynomial(b);
            Polynomial polynom3 = new Polynomial(c);
            int x = 5;

            WriteLine(polynom1);
            WriteLine($"Value for x=5 : {polynom1.GetValue(x)}");

            WriteLine($"Adds { polynom1 } to { polynom2 }: ");
            
            WriteLine(polynom1.Add(polynom1, polynom2));

            WriteLine($"Substracts {polynom3} from { polynom1}: ");
            WriteLine(polynom1.Substract(polynom1, polynom3));
            

            var mult = polynom1.Multiply(polynom1, polynom3);
            WriteLine($"Multiplying for { polynom1 } and { polynom3 }:");
            WriteLine(mult);
        }

        static void VectorDemo()
        {
            WriteLine("------Vector Demo--------");
            int[] a = { 2, 4, 5, 6 };
            Vector vec1 = new Vector(3, a);
            int[] b = { -1, 8, 3, 9 };
            Vector vec2 = new Vector(4, b);
            int[] c = { 2, 7, 1 };
            Vector vec3 = new Vector(2, c);

            try
            {
                WriteLine(vec1[vec1.StartIndex - 1]);
            }
            catch (IndexOutOfRangeException e)
            {
                WriteLine(e.Message);
            }
            WriteLine($"Try add {vec1} to {vec2}:");
            try
            {
                WriteLine(vec1.Add(vec1, vec2));
            }
            catch (ArgumentException e)
            {
                WriteLine(e.Message);
            }

            WriteLine($"Try add {vec1} to {vec3}:");
            try
            {
                WriteLine(vec1.Add(vec1, vec3));
            }
            catch (ArgumentException e)
            {
                WriteLine(e.Message);
            }

            WriteLine($"Substracts {vec2} from {vec1}:");
            WriteLine(vec1.Substract(vec1, vec2));

            WriteLine($"Multiplies {vec3} on {2}:");
            WriteLine(vec3.Multiply(2));
        }

        static void MatrixDemo()
        {
            WriteLine("------Matrix demo------");
            double[,] a = { { 2, 3, 5 }, { -1, 4, 7 }, { 3, 5, 6} };
            Matrix matr1 = new Matrix(a);
            double[,] b = { { 2, -1}, { 4, 6 }, { 7, 1} };
            double[,] c = { { 2, 4, 8 }, { -2, 6, 3 } };
            Matrix matr2 = new Matrix(b);
            Matrix matr3 = new Matrix(c);
            double[,] d = { { 4, 5, 1 }, { -3, -1, 5 }, { 9, 2, 1 } };
            Matrix matr4 = new Matrix(d);

            WriteLine($"Determinant for {matr1} =");
            WriteLine(matr1.GetDeterminant());

            WriteLine($"Multiply  {matr2} \n on {matr3} =");
            WriteLine(matr2.Multiply(matr2, matr3));

            WriteLine($"Add {matr1} to {matr4} =");
            WriteLine(matr1.Add(matr1, matr4));

            WriteLine($"Sustract {matr1} from {matr4} =");
            WriteLine(matr1.Sustract(matr4, matr1));

            WriteLine($"Second order minor for {matr1} = ");
            WriteLine($"{matr1.GetOrderMinor(2)}");

            WriteLine($"Additional second order minor for {matr1} =");
            WriteLine($"{matr1.GetAdditionalMinor(2)}");

            WriteLine($"Element[1,1] minor for {matr1} =");
            WriteLine($"{matr1.GetElementMinor(1, 1)}");
        }

        static void Main(string[] args)
        {
            //RectangleDemo();
            // PolynomialDemo();
            //VectorDemo();
            MatrixDemo();
            Read();
        }
    }
}
